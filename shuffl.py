import random
import sys

if __name__ == "__main__":
	file_name = sys.argv[1]
	data_list = []
	with open(file_name) as fr: 
		for line in fr:
			data_list.append(line.strip())	
	print(random.shuffle(data_list))
	with open(file_name+".shuffle", "w") as fw:
		for data in data_list:
			print(data, file=fw)
