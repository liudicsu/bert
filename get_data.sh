

rm -rf data
mkdir data

python shuffl.py  biaozhu_not_ok
python shuffl.py      biaozhu_ok
python shuffl.py chitchat_not_ok
python shuffl.py     chitchat_ok


#  32551 chitchat_not_ok.shuffle
data_file=chitchat_not_ok.shuffle  
train_num=6000
test_num=200
dev_num_t=400
dev_num_h=$test_num

head -n  $train_num $data_file                           >> data/train.tsv
tail -n  $test_num  $data_file                           >> data/test.tsv
tail -n  $dev_num_t $data_file  |head -n  $dev_num_h     >> data/dev.tsv


#  10362 biaozhu_not_ok.shuffle

data_file=biaozhu_not_ok.shuffle 
train_num=3000
test_num=200 
dev_num_t=400
dev_num_h=$test_num

head -n  $train_num $data_file                           >> data/train.tsv
tail -n  $test_num  $data_file                           >> data/test.tsv
tail -n  $dev_num_t $data_file  |head -n  $dev_num_h     >> data/dev.tsv


#  22961 chitchat_ok.shuffle

data_file=chitchat_ok.shuffle
train_num=8000
test_num=200 
dev_num_t=400
dev_num_h=$test_num

head -n  $train_num $data_file                           >> data/train.tsv
tail -n  $test_num  $data_file                           >> data/test.tsv
tail -n  $dev_num_t $data_file  |head -n  $dev_num_h     >> data/dev.tsv

#   1659 biaozhu_ok.shuffle


data_file=biaozhu_ok.shuffle 
train_num=1000
test_num=200 
dev_num_t=400
dev_num_h=$test_num

head -n  $train_num $data_file                           >> data/train.tsv
tail -n  $test_num  $data_file                           >> data/test.tsv
tail -n  $dev_num_t $data_file  |head -n  $dev_num_h     >> data/dev.tsv
