export BERT_BASE_DIR=/data2/muskliu/my_bert/chinese_L-12_H-768_A-12
export DATA_DIR=/data2/muskliu/my_bert/data
export TRAINED_CLASSIFIER=/data2/muskliu/my_bert/trained_model
export TEST_RESUTL=/data2/muskliu/my_bert/test_result

python run_classifier.py \
  --task_name=mydata \
  --do_predict=true \
  --data_dir=$DATA_DIR \
  --vocab_file=$BERT_BASE_DIR/vocab.txt \
  --bert_config_file=$BERT_BASE_DIR/bert_config.json \
  --init_checkpoint=$TRAINED_CLASSIFIER \
  --max_seq_length=128 \
  --output_dir=$TEST_RESUTL
