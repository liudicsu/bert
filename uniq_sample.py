import random
import sys

def _uniq_sample(sample):
    if len(sample) != 0:
        return random.choice(sample)

def uniq_sample():
    key = None
    cache_list = []
    for line in sys.stdin:
        sps = line.strip().split("\t")
        if key == sps[0]:  
            cache_list.append(line.strip()) 
        else:
            _s = _uniq_sample(cache_list)
            if _s:
                print _s
            uniq_sample = []
            key = sps[0]
    _s = _uniq_sample(cache_list)
    if _s:
        print(_s)

if __name__ == "__main__":
    uniq_sample()
