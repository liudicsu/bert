import random
import sys

if __name__ == "__main__":
    total_num = int(sys.argv[1])
    sample_num = int(sys.argv[2])
    thres = sample_num*1.0/total_num
    for line in sys.stdin:
        if random.random() < thres: 
            print line.strip()
      
